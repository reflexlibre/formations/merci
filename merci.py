import os

# Definition du message
message = "MERCI !!! "

# Récupération de la position de l'ordinateur à partir de son IP
position = int(os.popen('bash ./position.sh').read())

# On récupère le reste de la division de i par la longueur du message
k = position % len(message)

for i in range(0,2):
    # Pour chaque lettre du message
    for c in message:
        if k > 0:
            # Si i est supérieur à 0, enlever 1 à i
            k -= 1
        else:
            # Sinon, afficher la lettre contenue dans la variable c
            os.system(f"timeout 4 display -immutable '{c}.png' &")
            os.system("sleep 3")
